//
//  poker_gameTests.swift
//  poker gameTests
//
//  Created by Yveslym on 9/8/21.
//

import XCTest
@testable import poker_game

class poker_gameTests: XCTestCase {

   
    
    override func setUpWithError() throws {
      
    }


    // -> MARK: TEST HAND DETECTION
    
    func testRoyalFlushHand(){
        let royalCard = [Card(suit: .clubs, rank: .Ace), Card(suit: .clubs, rank: .King), Card(suit: .clubs, rank: .Queen), Card(suit: .clubs, rank: .Jack), Card(suit: .clubs, rank: .Ten)]
        let royalHand = Hand(cards: royalCard)
        
        XCTAssert(royalHand.pokerHand == .RoyalFlush)
    }
    
    func testStraightFlushHand(){
        let straightFlushCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .diamonds, rank: .Eight), Card(suit: .diamonds, rank: .Nine)]
        
        let straightFlushHand = Hand(cards: straightFlushCard)
        XCTAssert(straightFlushHand.pokerHand == .StraightFlush)
    }
    
    func testFourKindHand() {
        let fourKindCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .clubs, rank: .Five),Card(suit: .hearts, rank: .Five),Card(suit: .spades, rank: .Five), Card(suit: .diamonds, rank: .Jack)]
        
        let fourKindHand = Hand(cards: fourKindCard)
        
        XCTAssert(fourKindHand.pokerHand == .FourKind)
    }
    
    func testFullHouseHand() {
        
        let fullHouseCard  = [Card(suit: .clubs, rank: .King),Card(suit: .diamonds, rank: .King),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .King)]
        
        let fullHouseHand = Hand(cards: fullHouseCard)
        XCTAssert(fullHouseHand.pokerHand == .FullHouse)
    }
    
    func testStraightHand() {
        let straightCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .spades, rank: .Eight), Card(suit: .hearts, rank: .Nine)]
        let straightHand = Hand(cards: straightCard)
        
        XCTAssert(straightHand.pokerHand == .Straight)
    }
    
    func testThreeKindHand() {
        let threeKindCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        
        let threeKindHand = Hand(cards: threeKindCard)
        
        XCTAssert(threeKindHand.pokerHand == .ThreeKind)
    }
    
    func testTwoPairHand(){
        let twoPairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        let twoPairHand = Hand(cards: twoPairCard)
        
        XCTAssert(twoPairHand.pokerHand == .TwoPair)
    }
    
    func testOnePairHand(){
        let onePairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        
        let onePairHand = Hand(cards: onePairCard)
        
        XCTAssert(onePairHand.pokerHand == .OnePair)
    }
    func testHighHand() {
        let highCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Queen),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        let highHand = Hand(cards: highCard)
        
        XCTAssert(highHand.pokerHand == .HighCard)
    }
    
    
    // -> MARK: TEST GAME WINNER FROM ROYAL FLUSH HAND TO HIGH HAND
    
    func testRoyalFlushHandWin() {
        let royalCard = [Card(suit: .clubs, rank: .Ace), Card(suit: .clubs, rank: .King), Card(suit: .clubs, rank: .Queen), Card(suit: .clubs, rank: .Jack), Card(suit: .clubs, rank: .Ten)]

        let straightFlushCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .diamonds, rank: .Eight), Card(suit: .diamonds, rank: .Nine)]
        
        let royalHand = Hand(cards: royalCard)
        let straightFlushHand = Hand(cards: straightFlushCard)
        
        let game = Game(firstHand: royalHand, secondHand: straightFlushHand)
        XCTAssert(game.result.winner!.pokerHand.toInt() == royalHand.pokerHand.toInt())
    }
    
    func testStraightFlushWin() {
        let straightFlushCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .diamonds, rank: .Eight), Card(suit: .diamonds, rank: .Nine)]

        let fourKindCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .clubs, rank: .Five),Card(suit: .hearts, rank: .Five),Card(suit: .spades, rank: .Five), Card(suit: .diamonds, rank: .Jack)]
        
        let straightFlushHand = Hand(cards: straightFlushCard)
        let fourKindHand = Hand(cards: fourKindCard)
        
        let poker = Game(firstHand: straightFlushHand, secondHand: fourKindHand)
        
        
        XCTAssert(poker.result.winner!.pokerHand.toInt() == straightFlushHand.pokerHand.toInt())
    }
    
    func testFourKindWin() {
        let fourKindCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .clubs, rank: .Five),Card(suit: .hearts, rank: .Five),Card(suit: .spades, rank: .Five), Card(suit: .diamonds, rank: .Jack)]

        let fullHouseCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .clubs, rank: .Five),Card(suit: .hearts, rank: .Five),Card(suit: .spades, rank: .Jack), Card(suit: .diamonds, rank: .Jack)]

        let fourKindHand = Hand(cards: fourKindCard)
        let fullHouseHand = Hand(cards: fullHouseCard)
        
        let poker = Game(firstHand: fourKindHand, secondHand: fullHouseHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == fourKindHand.pokerHand.toInt())
    }
    func testFullHouseHandWin() {
       
        let fullHouseCard  = [Card(suit: .clubs, rank: .King),Card(suit: .diamonds, rank: .King),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .King)]
        
        let flushCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Ace),Card(suit: .diamonds, rank: .Two), Card(suit: .diamonds, rank: .Jack)]
        
        let fullHouseHand = Hand(cards: fullHouseCard)
        let flushHand = Hand(cards: flushCard)
        
        let poker = Game(firstHand: fullHouseHand, secondHand: flushHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == fullHouseHand.pokerHand.toInt())
    }
    func testFlushHandWin() {
        let flushCard = [Card(suit: .diamonds, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Ace),Card(suit: .diamonds, rank: .Two), Card(suit: .diamonds, rank: .Jack)]

        let straightCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .spades, rank: .Eight), Card(suit: .hearts, rank: .Nine)]

        let flushHand = Hand(cards: flushCard)
        let straightHand = Hand(cards: straightCard)

        let poker = Game(firstHand: flushHand, secondHand: straightHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == flushHand.pokerHand.toInt())
    }
    
    func testStraightHandWin(){
        let straightCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .diamonds, rank: .Seven),Card(suit: .spades, rank: .Eight), Card(suit: .hearts, rank: .Nine)]

        let threeKindCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        let straightHand = Hand(cards: straightCard)
        let threeKindHand = Hand(cards: threeKindCard)

        let poker = Game(firstHand: straightHand, secondHand: threeKindHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == straightHand.pokerHand.toInt())
    }
    func testThreeKindHandwin(){
        let threeKindCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Six),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let twoPairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let threeKindHand = Hand(cards: threeKindCard)
        let twoPairHand = Hand(cards: twoPairCard)

        let poker = Game(firstHand: threeKindHand, secondHand: twoPairHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == threeKindHand.pokerHand.toInt())

    }

    func tesTwoPairHandWin() {
        let twoPairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .Six),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let onePairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let twoPairHand = Hand(cards: twoPairCard)
        let onePairHand = Hand(cards: onePairCard)

        let poker = Game(firstHand: twoPairHand, secondHand: onePairHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == twoPairHand.pokerHand.toInt())
    }

    func testOnePairHandwin(){
        let onePairCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Five),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        let highCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Queen),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let onePairHand = Hand(cards: onePairCard)
        let highHand = Hand(cards: highCard)

        let poker = Game(firstHand: onePairHand, secondHand: highHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == onePairHand.pokerHand.toInt())
    }

    func testHighHandWin(){
        let highCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Queen),Card(suit: .spades, rank: .King),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]

        let lowHighCard  = [Card(suit: .clubs, rank: .Five),Card(suit: .diamonds, rank: .Queen),Card(suit: .spades, rank: .Two),Card(suit: .hearts, rank: .Six), Card(suit: .hearts, rank: .Nine)]
        let highHand = Hand(cards: highCard)
        let lowerHighHand = Hand(cards: lowHighCard)

        let poker = Game(firstHand: highHand, secondHand: lowerHighHand)
        XCTAssert(poker.result.winner!.pokerHand.toInt() == highHand.pokerHand.toInt())
    }
}
