//
//  ViewController.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import UIKit
import Combine

class ViewController: UIViewController {
    
    var viewModel = GameViewModel()
    private var cancellable = Set<AnyCancellable>()
    @IBOutlet weak var firstHandPlayerStackView: UIStackView!
    @IBOutlet weak var secondHandPlayerStackView: UIStackView!
    @IBOutlet weak var firstPlayerName: UILabel!
    @IBOutlet weak var secondPlayerName: UILabel!
    @IBOutlet weak var firstPlayerScore: UILabel!
    @IBOutlet weak var secondPlayerScore: UILabel!
    @IBOutlet weak var gameResultDescription: UILabel!
    @IBOutlet weak var winnerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
        initialConfig()
    }
    
    
    func bindViewModel () {
        
        viewModel.shouldUpdateScore
            .receive(on: DispatchQueue.main)
            
            .sink { [weak self] (update) in
                
                let result = update.0
                guard let self = self else { return }
                
                switch result.type {
                
                case .win: 
                    self.firstPlayerScore.text = String(self.viewModel.player1.score)
                    self.secondPlayerScore.text = String(self.viewModel.player2.score)
                    self.gameResultDescription.text = result.description
                    self.winnerLabel.text = "\(update.1) won this turn"
                    
                case .tie:
                    self.gameResultDescription.text =  "Both player have \(update.1) hand, it's a tie game"
                    self.winnerLabel.text = result.description
                       
                }
            }
            .store(in: &cancellable)
        
        //update card distribution
        viewModel.shouldDistributeCard
            .receive(on: DispatchQueue.main)
            
            .sink { [weak self] (hands) in
                
                guard let self = self else { return }
                
                
                let firstHand = hands[0]
                let secondHand = hands[1]
                
                let firstHandCardNames = firstHand.cards.map{$0.imageName}
                let secondHandCardNames = secondHand.cards.map{$0.imageName}
                
                let firstHandCardView = CardView(cardNames: firstHandCardNames)
                let secondHandCardView = CardView(cardNames: secondHandCardNames)
                
                self.firstHandPlayerStackView.removeFullyAllArrangedSubviews()
                self.secondHandPlayerStackView.removeFullyAllArrangedSubviews()
                
                for view in firstHandCardView.setHandView() {
                    self.firstHandPlayerStackView.addArrangedSubview(view)
                }
                for view in secondHandCardView.setHandView() {
                    self.secondHandPlayerStackView.addArrangedSubview(view)
                }
            }
            .store(in: &cancellable)
        
        viewModel.shouldBeginNewGame
            .receive(on: DispatchQueue.main)
            .sink {[weak self] summary in
                
                guard let self = self else { return }
                
                self.gameResultDescription.text = summary
                self.winnerLabel.text = "End of this game turn"
            }
            .store(in: &cancellable)
        
    }
    
    func initialConfig() {
        self.firstPlayerName.text = viewModel.player1.name
        self.secondPlayerName.text = viewModel.player2.name
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton){
        self.viewModel.launch()
    }
}

