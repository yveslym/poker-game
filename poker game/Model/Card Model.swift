//
//  Card Model.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import Foundation

enum Suit: String, CaseIterable {
    case clubs
    case diamonds
    case hearts
    case spades
}

enum Rank: String, CaseIterable {
    case Ace = "Ace"
    case Two = "Two"
    case Three = "Three"
    case Four = "Four"
    case Five = "Five"
    case Six = "Six"
    case Seven = "Seven"
    case Eight = "Eight"
    case Nine = "Nine"
    case Ten = "Ten"
    case Jack = "Jack"
    case Queen = "Queen"
    case King = "King"
    
    func toInt() -> Int {
        switch self {
        
        case .Ace: return 14
            
        case .Two: return 2
        case .Three:  return 3
            
        case .Four: return 4
            
        case .Five: return 5
            
        case .Six: return 6
            
        case .Seven: return 7
            
        case .Eight: return 8
            
        case .Nine: return 9
            
        case .Ten: return 10
            
        case .Jack: return 11
            
        case .Queen: return 12
            
        case .King: return 13
        }
    }
}

struct Card {
    let suit: Suit
    let rank: Rank
    var imageName: String
}

extension Card: CustomStringConvertible {
    var description: String {
        return rank.rawValue.capitalized + " of " + self.suit.rawValue.capitalized
    }
}

