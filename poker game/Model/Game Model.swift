//
//  Game Model.swift
//  poker game
//
//  Created by Yveslym on 9/9/21.
//

import Foundation


struct Game {
    var firstHand: Hand
    var secondHand: Hand
    var result: GameResult
    
    init(firstHand: Hand, secondHand: Hand) {
        self.firstHand = firstHand
        self.secondHand = secondHand
        self.result = GameResult(winner: nil, looser: nil, type: .tie(hand: ""))
        self.result = self.gameResult()
       
    }

    /// return the game winner.
    private mutating func gameResult() -> GameResult{
        
        // check if both hands bad card
        if firstHand.rank == .HighCard && secondHand.rank == .HighCard {
            
            let firstHandResult = firstHand.cards.map{$0.rank.toInt()}.max()
            let secondHandResult = secondHand.cards.map{$0.rank.toInt()}.max()
            
                // both high card have the same highest card -> Tie Game
            if ( firstHandResult == secondHandResult){
                return GameResult(winner: nil, looser: nil, type: .tie(hand: firstHand.rank.description))
            }
            // either high card have the highest card -> one winner
            else {
            return firstHandResult! > secondHandResult! ? GameResult(winner: firstHand, looser: secondHand, type: .win) : GameResult(winner: secondHand, looser: firstHand, type: .win)
            }
        }
        
        // check other tie game between powerfull hands
        else if firstHand.rank == secondHand.rank{
            
            let firstHandResult = firstHand.cards.map{$0.rank.toInt()}.reduce(0,+)
            let secondHandResult = secondHand.cards.map{$0.rank.toInt()}.reduce(0,+)
            
            // perfect tie -> Tie game
            if firstHandResult == secondHandResult {
                return GameResult(winner: nil, looser: nil, type: .tie(hand: ("\(firstHand.rank.description)")))
            }
            // strongest hand win
            else {
                return firstHandResult > secondHandResult ?  GameResult(winner: firstHand, looser: secondHand, type: .win) : GameResult(winner: secondHand, looser: firstHand, type: .win)
            }
        }
        // check which hand is stronger -> one winner
        else {
        return firstHand.rank.toInt() > secondHand.rank.toInt() ?  GameResult(winner: firstHand, looser: secondHand, type: .win) : GameResult(winner: secondHand, looser: firstHand, type: .win)
        }
    }
}

extension GameResult: CustomStringConvertible {
    
    var description: String {
        
        if let winner = winner {
        
        switch winner.rank {
        
        case .BadCards: fallthrough
            
        case .HighCard:
            let winCard = winner.cards.sorted(by: {$0.rank.toInt() > $1.rank.toInt()}).first
            let lostCard = looser!.cards.sorted(by: {$0.rank.toInt() > $1.rank.toInt()}).first
            return winner.rank.description + " " + winCard!.description + " win over " + looser!.rank.description + " " + lostCard!.description
        case .OnePair: fallthrough
            
        case .TwoPair: fallthrough
            
        case .ThreeKind: fallthrough
            
        case .Straight: fallthrough
            
        case .Flush: fallthrough
            
        case .FullHouse: fallthrough
            
        case .FourKind: fallthrough
            
        case .StraightFlush: fallthrough
            
        case .RoyalFlush:
            return winner.rank.description + " card win over " + looser!.rank.description
        }
    }
        else { return "it's a tie game"}
       
    }
}

    struct GameResult {
        var winner: Hand?
        var looser: Hand?
        let type: GameResultType
        
        
        enum GameResultType {
            case win
            case tie (hand: String)
        }
    }

