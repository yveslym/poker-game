//
//  Hand Model.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import Foundation

struct Hand {
    let id = UUID().uuidString
    let cards: [Card]
    var rank: HandRank {
        get {_rank()}
        set (newValue) {newValue}
    }
   
    
    init (cards: [Card]) {
        self.cards = cards.sorted(by: { (first, second) -> Bool in
            return first.rank.toInt() > second.rank.toInt()
        })
        rank = _rank()
    }
    // check weither if the hand is straight flush and the sum off the card is 60
     func isRoyalFlush() ->  Bool {
        
        if isStraightFlush() == true {
           
            return cards.compactMap{$0.rank.toInt()}.reduce(0,+) == 60 ? true : false
        }
        else {return false}
    }
    
    // check if the hand is flush and straight at the same time,
    // five cards in a row, all in the same suit.
     func isStraightFlush() -> Bool {
        return isFlush() == true && isStraight() == true
}
    // check if the same card in each of the four suits.
    func isFourKind() -> Bool{
        let rank = cards.map{ $0.rank.toInt()}
        return rank.occurrences.contains(where: {$0.value == 4})
    }
    // check weither the hand is two pair and three of a kind at the same time
    func isFullHouse() -> Bool {
        
        return isTwoPair() && isThreeKind() ? true : false
    }
   
    // check if the cards in the hand have same suits
    func isFlush() -> Bool {
        let suits = cards.map{ $0.suit.rawValue}
        let suitsOccurency = suits.occurrences.contains(where: {$0.value == 5})
        return suitsOccurency
    }
    
    // check if the cards are in numerical order
    func isStraight() -> Bool {
        let ranks = cards.map{ $0.rank.toInt() }.sorted()
        return ranks.map { $0 - 1 }.dropFirst() == ranks.dropLast() ? true : false
    }
    
    // check if tree of same rank and two non-paired cards.
    func isThreeKind() -> Bool {
        let occurences = cards.map{$0.rank.rawValue}.occurrences
        return occurences.contains(where: {$0.value >= 3})
    }
    
    // check if two different pairings or sets of the same card in one hand
    func isTwoPair() -> Bool {
        let ranksOccurences = cards.map{$0.rank.rawValue}.occurrences
        let pairs = ranksOccurences.filter{$0.value >= 2}
        return pairs.count == 2
    }
    // check if a pairing of the same card
    func isOnePair() -> Bool {
        let ranks = cards.map{$0.rank.rawValue}
        let pair = ranks.occurrences.contains(where: {$0.value >= 2})
        return pair
    }
    // check if hand has no matching cards
    func isHighCard() -> Bool {
        let ranks = cards.map{$0.rank.rawValue}
        let suits = cards.map{$0.suit.rawValue}
        
        return Set(ranks).count == 5 && Set(suits).count > 1
    }
    
    fileprivate func _rank() -> HandRank {
        
        if isRoyalFlush() { return .RoyalFlush}
        else if isStraightFlush(){return .StraightFlush}
        else if isFourKind() {return .FourKind}
        else if isFullHouse() {return .FullHouse}
        else if isFlush() {return .Flush}
        else if isStraight() {return .Straight}
        else if isThreeKind(){return .ThreeKind}
        else if isTwoPair() {return .TwoPair}
        else if isOnePair() {return .OnePair}
        else if isHighCard() {return .HighCard}
        
        else {return .BadCards}
    }
}

enum HandRank: String, CaseIterable, Comparable{

    case RoyalFlush = "Royal Flush"
    case StraightFlush = "Straight Flush"
    case FourKind = "Four of a Kind"
    case FullHouse = "Full House"
    case Flush = "Flush"
    case Straight = "Staight"
    case ThreeKind = "Three of a Kind"
    case TwoPair = "Two Pair"
    case OnePair = "One Pair"
    case HighCard = "High Card"
    case BadCards = "Bad"
    
    static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.rawValue > rhs.rawValue
      }
    
    func toInt() -> Int {
        switch self {
        
        case .BadCards: return 0
            
        case .HighCard: return 1
            
        case .OnePair: return 2
            
        case .TwoPair: return 3
            
        case .ThreeKind: return 4
            
        case .Straight: return 5
            
        case .Flush: return 6
            
        case .FullHouse: return 7
            
        case .FourKind: return 8
            
        case .StraightFlush: return 9
            
        case .RoyalFlush: return 10
            
        }
    }
    
    
}

extension HandRank: CustomStringConvertible {
    var description: String {
        return self.rawValue
    }
}
