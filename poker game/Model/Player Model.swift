//
//  Player Model.swift
//  poker game
//
//  Created by Yveslym on 9/9/21.
//

import Foundation

struct Player {
    let name: String
    var hands: [Hand]
    var score: Int
    
    init(name: String) {
        self.name = name
        self.hands = [Hand]()
        self.score = 0
    }
}
