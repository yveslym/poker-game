//
//  Card View.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import Foundation
import UIKit
struct CardView {
    
    let cardNames: [String]
    
    func setHandView() -> [UIImageView]{
        var handImageViews = [UIImageView]()
        
        for name in cardNames {
            let image = UIImage(named: name)
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            handImageViews.append(imageView)
            
        }
        
        return handImageViews
    }
}
