//
//  App+Extensions.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import Foundation
import UIKit


extension Array where Element: Hashable {
    var occurrences: [Element:Int] {
        return reduce(into: [:]) { $0[$1, default: 0] += 1 }
    }
}


extension UIStackView {
    
    func removeFully(view: UIView) {
        removeArrangedSubview(view)
        view.removeFromSuperview()
    }
    
    func removeFullyAllArrangedSubviews() {
        arrangedSubviews.forEach { (view) in
            removeFully(view: view)
        }
    }
    
}
