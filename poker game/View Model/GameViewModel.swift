//
//  GameViewModel.swift
//  poker game
//
//  Created by Yveslym on 9/8/21.
//

import Foundation
import Combine

class GameViewModel: ObservableObject, Identifiable {
    
    fileprivate var games: [GameResult?]
    fileprivate var deck: [Card]
    
    private (set) var player1 = Player(name: "Yves")
    private (set) var player2 =  Player (name: "Ashu")
    
    // game state
    var shouldUpdateScore = PassthroughSubject <(GameResult, String), Never>()
    var shouldDistributeCard = PassthroughSubject <[Hand], Never>()
    var shouldBeginNewGame = PassthroughSubject<String,Never>()
   
    init() {
        let deck = Suit.allCases.flatMap { suit in
            Rank.allCases.map { rank in
                Card(suit: suit, rank: rank, imageName: String(rank.rawValue + suit.rawValue))
            }
        }
        self.deck = deck.shuffled()
        
        self.games = [GameResult]()
    }
    
    fileprivate func refillDeck() {
        let deck = Suit.allCases.flatMap { suit in
            Rank.allCases.map { rank in
                Card(suit: suit, rank: rank, imageName: String(rank.rawValue + suit.rawValue))
            }
        }
        self.deck = deck.shuffled()
    }
    
    func launch() { self.gamePlay() }
    
    fileprivate func beginNewGame(){
        refillDeck()
        
        if player1.score > player2.score {
            
        let summary = "the game is over. \(player1.name) won by \(player1.score) over \(player2.name) with \(player2.score)"
            shouldBeginNewGame.send(summary)
        }
        else if player1.score == player2.score {
            let summary = "the game is over, this is a tie game, press play to play again"
            shouldBeginNewGame.send(summary)
        }
        else {
            let summary = "the game is over. \(player2.name) won by \(player2.score) over \(player1.name) with \(player1.score)"
            shouldBeginNewGame.send(summary)
        }
        player1.score = 0
        player2.score = 0
    }
    fileprivate func gamePlay() {
        
        if shoudBeginNewGame() == true {
            beginNewGame()
            return
        }
        
        var firstHandCard = [Card]()
        var secondHandCard = [Card]()
        
       
        
        for i in 1...10 {
            if i % 2 == 0{
                let card = deck.removeFirst()
                firstHandCard.append(card)
            }
            else {
                let card = deck.removeFirst()
                secondHandCard.append(card)
            }
        }
        let firstHand = Hand(cards: firstHandCard)
        let secondHand = Hand(cards: secondHandCard)
        
        self.shouldDistributeCard.send([firstHand,secondHand])
        
        player1.hands.append(firstHand)
        player2.hands.append(secondHand)
        
        let game = Game(firstHand: firstHand, secondHand: secondHand)
        
        let result = game.result
        
       
        switch game.result.type {
        
        case .win:
            if player1.hands.contains(where: {$0.id == result.winner!.id}){
                player1.score += 1
                self.shouldUpdateScore.send((result, player1.name))
            }
            else {
                player2.score += 1
                self.shouldUpdateScore.send((result, player2.name))
                }
        case .tie(hand: let hand):
            self.shouldUpdateScore.send((result, hand))
        }
        
        self.games.append(result)
    }
    
    fileprivate func shoudBeginNewGame()-> Bool{
        return deck.count == 2 ? true : false
    }
}


