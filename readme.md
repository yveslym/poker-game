Texas Hold'em
===


Overview
---

Working on this project was interesting because it gave me an opportunity to learn more about poker. Now, I'm pretty sure I'm ready to challenge the big poker players in Vegas. Sin City, here I come!

I spent a couple of hours doing my research to understand the concepts of the game as well as the winning pattern. I used different resources such as [cards chat](https://www.cardschat.com/poker-hands/) to understand the poker hand ranking system. Also, I should point out that I focused on the engine side to cover most of the edge cases and kept the UI minimal yet understandable.

#### Time Report:

you can find the time report [here](https://drive.google.com/file/d/11_L2KY8fjLhS-bmRGbeGfTG6wY53h0hQ/view?usp=sharing)

#### How to run:

First you can clone this repo, and on your xcode, you can can run the **app** on simulator (only iOS), Also you can run the **test** for the engine **Poker Hand** detection and **Game Rules.**

Engine
---

The engine rules are based on [cards chat](https://www.cardschat.com/poker-hands/) which cover more rules than what was given for this project. Overall, there are 10 ranks (the bad rank is just a default case) where the highest is Royal Flush and the lowest is High Card. In the spirit of the wild west, the strongest hand beats the lowest.


| Hand Rank | Kind (Rank) | Suits |
| -------- | -------- | -------- |
| ![](https://i.imgur.com/mxBd5c9.png)     |  ![](https://i.imgur.com/DQajrVz.png)    | ![](https://i.imgur.com/lCThiYq.png)     |


The engine is based on detecting which rank is the current hand first and then compares it with the opponent's hand to determine the winner of the game.

**Process:**
First, the Hand struct determines the rank. To accomplish this, I used a different array manipulation for each scenario. For example, if a hand is "THREE OF A KIND," I get the occurrences of the cards (how many times a card is repeated in a hand) and return true if one kind is repeated 3 times.

![](https://i.imgur.com/bLr5MoC.png)

After finding the rank, I can compare both hands to determine the winner of the game.

I also covered some edge cases where some Players' Hands might have the same rank or kind where it can be a perfect tie game. On some occasions, their Hands could be the same but have a different kind (King, Jack, Queen, etc.) -- the engine will be able to determine which rank is higher and therefore the winner.

UI
---
As I mentioned in the introduction, I kept the UI very minimal.

![](https://i.imgur.com/nS92zPH.png)

The top part is the opponent's side which shows their name and current hand. The middle part is the result of the ongoing game and the lower part is the player side. Due to the short timeframe for this project, I didn't spend some time adding animations or sounds when winning or losing. Those are features that could be added in if preferred.

This game was built with UIKit since I'm more proficient with UIKit than any other framework. The Auto Layout is all relative so it is automatically optimized for all screen sizes. Technically, I'm proficient programmatically but due to time constraints, I decided to use Storyboard.



### Win and Lose Situation


| Three of a Kind Win | High Card King Win Over Queen | Tie Game, both High hand with King |
| -------- | -------- | -------- |
|  ![](https://i.imgur.com/86ieofd.png)   |   ![](https://i.imgur.com/VoKHifI.png)   | ![](https://i.imgur.com/0GR7Ql4.png)     |


Design Pattern
---
In terms of design pattern, I decided to use MVVM with a single GameViewModel which handles the game mechanics and updates the view for the winning and losing results.

Improvement
---
The current app is less than an MVP, the UI is minimal and there's no data record (as it wasn't required), nor creating an account or input the name. The project was solely focused on getting the engine to work and for it to work smoothly. Some improvements would be: Saving the game state so the user can resume the game when they want to; Having a database that holds the record of all the games and current user top scores; Having a list of all top scoring players; Online real-time game played with other users; The capability to share your current scores on social media.

Important Note
---
There's a test coverage for each hand detection as well as a game result. The game is easy to run through cloning the repo and running it locally.

Side Note
---
 **Hand**: Composed of 5 cards
 
 **Hands Rank**: 10 different rankings for a Hand (from Royal to High)
 
 **Rank/Kind**: Card kind (King, Queen, Jack, etc.)
 
 **Suits**: 4 suits (Spade, Heart, Club, Diamond)
 
